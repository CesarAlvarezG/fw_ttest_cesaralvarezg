#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

/*
    Proyecto de Internet de la Cosas (IoT) para evaluacion laboral
    Autor: Cesar Augusto Alvarez Gaspar
    Version: 0.1
    Fecha: 10 de septiembre de 2022
*/
//Enumeración para determinar el origen del mensaje
enum origen_e{fundicion_e,modelado_e,prensa_e,deposito_e};
//Estructura para almacenar la información entregada por el PAC3200
struct telemetria_t
{
    float VoltajeL1N;
    float VoltajeL2N;
    float VoltajeL3N;
    float CurrentL1;
    float CurrentL2;
    float CurrentL3;
    double CumEneryImp;
    float PowerFactorL1;
    float PowerFactorL2;
    float PowerFactorL3;
    int Proceso;
}telemetria={0,0,0,0,0,0,0,0,0,0,fundicion_e};

void mensaje_inicial(void);
void leerPAC3200(void);
void monitorPAC3200(void);//Mostrar en monitor la lectura del PAC
void simularMedidaPAC3200(void);
void acondicionar(void);


//-----------------------------------Funcion Principal--------------------------------------------------------
void app_main() 
{
    long nMensajes=0;
    mensaje_inicial();
    while(1)
    {
        leerPAC3200();
        acondicionar();
        printf("Numero de mensajes: %ld\n\n",nMensajes);
        nMensajes++;
        vTaskDelay(5000/portTICK_RATE_MS);//Asignacion de frecuencia de muestreo de 5 seg
    }
}

//-----------------------------------Funciones Secundarias----------------------------------------------------

void mensaje_inicial(void)
{
    printf("Bienvendo al sistema de monitoreo de consumo de potencia electrica\n");
}

void leerPAC3200(void)
{
 simularMedidaPAC3200();
 monitorPAC3200();
}

void monitorPAC3200(void)
{
    printf("Telemetria en ");
    switch (telemetria.Proceso)
    {
    case fundicion_e:
        printf("la fundicion\n");
        break;
    case modelado_e:
        printf("el modelado\n");
        break;
    case prensa_e:
        printf("la prensa neumatica\n");
        break;
    case deposito_e:
        printf("el deposito\n");
        break;
    default:
        printf("el sistema\n");
        break;
    }
    printf("\tVoltaje en linea 1: %f\n",telemetria.VoltajeL1N);
    printf("\tVoltaje en linea 2: %f\n",telemetria.VoltajeL2N);
    printf("\tVoltaje en linea 3: %f\n",telemetria.VoltajeL3N);
    printf("\tCorriente en linea 1: %f\n",telemetria.CurrentL1);
    printf("\tCorriente en linea 2: %f\n",telemetria.CurrentL2);
    printf("\tCorriente en linea 3: %f\n",telemetria.CurrentL3);
    printf("\tEnergia acumulada: %f\n",telemetria.CumEneryImp);
    printf("\tFactor de potencia en linea 1: %f\n",telemetria.PowerFactorL1);
    printf("\tFactor de potencia en linea 2: %f\n",telemetria.PowerFactorL2);
    printf("\tFactor de potencia en linea 3: %f\n",telemetria.PowerFactorL3);
}

void simularMedidaPAC3200(void)
{
    telemetria.VoltajeL1N=random()%220;
    telemetria.VoltajeL2N=random()%220;
    telemetria.VoltajeL3N=random()%220;
    telemetria.CurrentL1=random()%10;
    telemetria.CurrentL2=random()%10;
    telemetria.CurrentL3=random()%10;
    telemetria.CumEneryImp+=random()%10;
    telemetria.PowerFactorL1=(float)(random()%1000)/1000;
    telemetria.PowerFactorL2=(float)(random()%1000)/1000;
    telemetria.PowerFactorL3=(float)(random()%1000)/1000;
}

void acondicionar(void)
{
    /*Funcion diseñada para adaptar los datos no esten un formato util para la telemetria.
    En este caso los datos se han acondicionado en el simulador y por eso, queda vacia.
    */
}